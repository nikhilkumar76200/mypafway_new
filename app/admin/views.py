from flask import Blueprint
from flask.views import MethodView

admin_auth = Blueprint('admin', __name__)


class LoginView(MethodView):
    pass


class LogOutView(MethodView):
    pass


# # # Creating View Function/Resources
LogInView = LoginView.as_view('loginview')
LogOutView = LogOutView.as_view('logoutview')

# # # adding routes to the Views we just created
admin_auth.add_url_rule('/admin/login', view_func=LogInView, methods=['POST'])
admin_auth.add_url_rule('/admin/logout', view_func=LogOutView, methods=['GET'])
