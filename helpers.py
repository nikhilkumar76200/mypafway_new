import datetime
import json
import logging
import smtplib
import ssl
import threading
import time
from email.message import EmailMessage

import jwt


def email_notification(report):
    try:
        msg = EmailMessage()
        msg.set_content(json.dumps(report))
        msg["Subject"] = " NEWS Alert "
        msg["From"] = "xlrt.prod.news@gmail.com"
        msg["To"] = "shakya.dutta@ivanwebsolutions.com"
        context = ssl.create_default_context()
        with smtplib.SMTP("smtp.gmail.com", port=587) as smtp:
            smtp.starttls(context=context)
            smtp.login(msg["From"], "ymzs epbb hbqk sxij")
            smtp.send_message(msg)
        response = {"status": 'success', "message": 'message sent through email'}
        return response
    except Exception as e:
        response = {"status": 'error', "message": f'{str(e)}'}
        return response


def event(event_name):
    def decorator(method):
        def timer(*args, **kwargs):
            _event_name = event_name.replace(" ", "_")
            logger = logging.getLogger(_event_name)
            ex = None
            data = None
            result = "SUCCESS"
            begin = time.time()
            try:
                data = method(*args, **kwargs)
            except Exception as e:
                ex = e
                result = "FAILURE"
            end = time.time()
            response = json.dumps(
                {
                    "eventName": _event_name,
                    "status": result,
                    "eventTime": end - begin,
                    "metaData": {
                        "thread": threading.current_thread().ident,
                        "class": method.__class__.__name__,
                        "method": method.__name__,
                    },
                },
            )
            if ex:
                logger.error(response)
                raise ex
            logger.info(response)
            return data

        return timer

    return decorator


class GenerateApiToken:

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret

    def generate_token(self, client_id, client_secret):
        try:
            if self.client_id == client_id and self.client_secret == client_secret:

                payload = {
                    'id': self.client_id,  # client_id
                    'secret': self.client_secret,  # client_secret
                    # expire time of the jwt tokens (datetime.timedelta(days=5) expires in 5 days)
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(days=5),
                }
                # encoding the JWT Token
                jwt_token = {'Access_token': jwt.encode(payload, "SECRET", algorithm='HS256'), 'success': True}
                response = {"status": 'success', "message": 'JWT token created successfully', 'token': jwt_token}
                return response
            else:
                response = {"status": 'error', "message": 'JWT token created unsuccessfully due to invalid '
                                                          'client_secret or client_id'}
                return response

        except Exception as e:
            response = {"status": 'error', "message": f'{str(e)}'}
            return response


class ValidateApiToken:

    def __init__(self, headers, client_id, client_secret):

        self.client_id = client_id  # client_id
        self.client_secret = client_secret  # client_secret
        self.headers = headers

    def verify_token(self):

        try:
            # decoding the JWT Token
            decoded_token = jwt.decode(self.headers, 'SECRET', algorithms=['HS256'])
            # To verify data encode in GenerateApiToken == data encode in ValidateApiToken
            print(decoded_token)
            if decoded_token['id'] == self.client_id and decoded_token['secret'] == self.client_secret:
                return {"status": 'success', 'message': 'Access Granted'}
            else:
                return {"status": 'error', 'message': 'Access denied due invalid credentials'}
        except jwt.ExpiredSignatureError as e:
            response = {"status": 'error', "message": f'{str(e)}'}
            return response
